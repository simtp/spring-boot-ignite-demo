package org.simtp.demo.spring.boot.ignite.demo;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.configuration.CacheConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Separately configurable CacheConfiguration for Cache2
 */
@Configuration
@ConditionalOnProperty(value = "cache2.enabled", havingValue = "true", matchIfMissing = true)
public class Cache2Configuration {

    public static final String CACHE2_NAME = "cache2";

    @Bean
    public IgniteConfigurer cache2IgniteConfigurer() {
        return igniteConfiguration -> {
            CacheConfiguration<Long, String> cacheConfiguration = new CacheConfiguration<>(CACHE2_NAME);
            Utils.addCacheConfiguration(igniteConfiguration, cacheConfiguration);
        };
    }

    @Configuration
    @ConditionalOnProperty(value = "cache2.enabled", havingValue = "true", matchIfMissing = true)
    public static class Cache2PostIgnitionConfiguration {
        
        final private Ignite ignite;
        private static final Logger LOG = LoggerFactory.getLogger(Cache2PostIgnitionConfiguration.class);

        public Cache2PostIgnitionConfiguration(Ignite ignite) {
            this.ignite = ignite;
        }

        @Bean
        public IgniteCache<Long, String> cache2() {
            return ignite.cache(CACHE2_NAME);
        }

        @PostConstruct
        public void start() {
            Executors
                    .newScheduledThreadPool(1)
                    .scheduleWithFixedDelay(
                            () -> LOG.info(CACHE2_NAME + " size " + cache2().size()),
                            10,
                            10,
                            TimeUnit.SECONDS
                    );
        }

    }


}
