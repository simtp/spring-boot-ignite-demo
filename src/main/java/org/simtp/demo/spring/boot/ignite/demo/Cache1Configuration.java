package org.simtp.demo.spring.boot.ignite.demo;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Separately configurable CacheConfiguration for Cache1
 */
@Configuration
@ConditionalOnProperty(value = "cache1.enabled", havingValue = "true", matchIfMissing = true)
public class Cache1Configuration implements IgniteConfigurer {

    public static final String CACHE1_NAME = "cache1";

    @Override
    public void configureIgnite(IgniteConfiguration igniteConfiguration) {
        CacheConfiguration<Long, String> cacheConfiguration = new CacheConfiguration<>(CACHE1_NAME);
        Utils.addCacheConfiguration(igniteConfiguration, cacheConfiguration);
    }

    @Configuration
    @ConditionalOnProperty(value = "cache1.enabled", havingValue = "true", matchIfMissing = true)
    public static class Cache1PostIgnitionConfiguration {

        final private Ignite ignite;
        private static final Logger LOG = LoggerFactory.getLogger(Cache1Configuration.Cache1PostIgnitionConfiguration.class);

        public Cache1PostIgnitionConfiguration(Ignite ignite) {
            this.ignite = ignite;
        }

        @Bean
        public IgniteCache<Long, String> cache1() {
            return ignite.cache(CACHE1_NAME);
        }

        @PostConstruct
        public void start() {
            try {
                Executors
                        .newScheduledThreadPool(1)
                        .scheduleWithFixedDelay(
                                () -> LOG.info(CACHE1_NAME + " size " + cache1().size()),
                                10,
                                10,
                                TimeUnit.SECONDS
                        );
            } catch ( Exception ex) {
                ex.printStackTrace();
            }
        }

    }


}
