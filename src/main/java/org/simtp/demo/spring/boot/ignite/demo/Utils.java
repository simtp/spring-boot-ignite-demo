package org.simtp.demo.spring.boot.ignite.demo;

import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    /**
     * Helper utility to add a new CacheConfiguration to IgniteConfiguration without over writing existing ones.
     * @param igniteConfiguration The IgniteConfiguration
     * @param cacheConfiguration The CacheConfiguration to add
     * @param <K> The Key type of the Cache
     * @param <V> The Value type of the cache
     */
    public static <K, V> void addCacheConfiguration(IgniteConfiguration igniteConfiguration, CacheConfiguration<K, V> cacheConfiguration) {
        @SuppressWarnings("rawtypes")
        List<CacheConfiguration> cacheConfigurations = igniteConfiguration
                .getCacheConfiguration() == null
                ? new ArrayList<>() : new ArrayList<>(Arrays.asList(igniteConfiguration.getCacheConfiguration()));
        cacheConfigurations.add(cacheConfiguration);
        igniteConfiguration.setCacheConfiguration(cacheConfigurations.toArray(CacheConfiguration[]::new));
    }
}
