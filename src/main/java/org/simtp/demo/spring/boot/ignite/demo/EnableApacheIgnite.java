package org.simtp.demo.spring.boot.ignite.demo;

import org.springframework.context.annotation.Import;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(IgniteModularConfiguration.class)
public @interface EnableApacheIgnite {}