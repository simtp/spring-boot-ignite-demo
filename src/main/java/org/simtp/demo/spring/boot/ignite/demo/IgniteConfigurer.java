package org.simtp.demo.spring.boot.ignite.demo;

import org.apache.ignite.configuration.IgniteConfiguration;

public interface IgniteConfigurer {

    void configureIgnite(IgniteConfiguration igniteConfiguration);
}
