package org.simtp.demo.spring.boot.ignite.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Separate logging configuration for Ignite
 */
@Configuration
public class IgniteLoggingConfiguration {

    /*
        Override the default Ignite metrics logging by disabling metrics logging
     */
    @Bean
    public IgniteConfigurer igniteLoggingConfigurer() {
        return igniteConfiguration -> {
            igniteConfiguration.setMetricsLogFrequency(0);
        };
    }
}
