package org.simtp.demo.spring.boot.ignite.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//The following isn't strictly required as the IgniteModularConfiguration
//will be component scanned as it is in the same package, but it shows how
//it could be used outside of the project and in another library
@EnableApacheIgnite
public class SpringBootIgniteDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootIgniteDemoApplication.class, args);
    }

}


