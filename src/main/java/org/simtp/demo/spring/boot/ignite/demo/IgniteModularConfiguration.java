package org.simtp.demo.spring.boot.ignite.demo;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteSpring;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

/**
 * <p>
 *     This provides the ability to centrally configure Ignite, without having to have
 *     knowledge of all the caches etc that it will host.
 * </p>
 * <p>
 *     This class could be moved into a standalone library or starter module making it more suitable
 *     as a for a framework library across multiple applications.
 * </p>
 */
@Configuration
public class IgniteModularConfiguration implements ApplicationContextAware {

    public IgniteModularConfiguration(Set<IgniteConfigurer> igniteConfigurers) {
        this.igniteConfigurers = igniteConfigurers;
    }

    private ApplicationContext applicationContext;

    final private Set<IgniteConfigurer> igniteConfigurers;

    @Override
    public void setApplicationContext(
            @NotNull ApplicationContext applicationContext
    ) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Bean
    public IgniteConfiguration igniteConfiguration() {
        IgniteConfiguration config =  new IgniteConfiguration();
        igniteConfigurers.forEach(configurer -> configurer.configureIgnite(config));
        return config;
    }

    @Bean
    public Ignite ignite() throws IgniteCheckedException {
        return IgniteSpring.start(igniteConfiguration(), applicationContext);
    }
}

